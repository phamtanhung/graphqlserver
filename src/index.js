const { ApolloServer } = require('apollo-server');

const config = require('./config');
const typeDefs = require('./typedefs');
const resolvers = require('./resolvers');
const UsersAPI = require('./services/rest-service');
const { models, db } = require('./db');

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => {
    return {
      usersAPI: new UsersAPI()
    };
  },
  context: ({ req, res}) => {
    // console.log('operationName: ', req.body.operationName);
    // console.log('variables: ', req.body.variables);
    // console.log('query: ', req.body.query);

    const user = db.get('user').value()
    return { models, db }
  },
  // engine: {
  //   apiKey: config.ENGINE_API_KEY,
  //   schemaTag: config.SCHEMA_TAG
  // }
});

const PORT = process.env.PORT || 4000;
server.listen({ port: PORT }).then(({ url }) => {
  console.log(`Server is ready on ${url}`);
});