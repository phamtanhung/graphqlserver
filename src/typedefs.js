// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.

// AST is abstract syntax tree
const { userTypeDefs } = require('./modules/users');
const { projectTypeDefs } = require('./modules/projects');


module.exports = [userTypeDefs, projectTypeDefs];