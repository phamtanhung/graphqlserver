const fs = require('fs');
const dotenv = require('dotenv');

const environment = process.env.NODE_ENV || 'local';
let config = dotenv.config();

if (environment === 'local') {
  config = dotenv.parse(fs.readFileSync(`.env.${environment}`));
} else {
  config = process.env;
}
console.log('config env', config);

module.exports = config;