const userTypeDefs = require('./users.typedefs');
const userResolvers = require('./users.resolvers');

module.exports = {
  userTypeDefs,
  userResolvers
};