
const resolvers = {
  Query: {
    users: (parent, { input }, { models }) => {
      
      return models.User.findAll();
    },
    user(parent, {input}, { models }) {
      return models.User.findOne(input);
    },
    getRandomUser: async (_source, _args, { dataSources }) => {
      return dataSources.usersAPI.getRandomUser();
    },
    getMostUsers: async (_source, { input }, { dataSources }) => {
      const { limit } = input;
      return dataSources.usersAPI.getMostUsers(limit);
    }
  },
  User: {
    username: (user) => {
      
      if (!user) return '';
      if (!user.username) {
        return `${user.name.first} ${user.name.last}`;
      }
      return user.username;
    }
  },
  Mutation: {
    addUser: (parent, { input }, { models }) => {
      const newUser = models.User.create({ ...input })
      return newUser;
    },
    updateUser: (parent, { input }, { models }) => {
      const user = models.User.update({ ...input })
      return user;
    },
    removeUser: (parent, { input }, { models }) => {
      const user = models.User.remove({ ...input });
      return user;
    }
  }
}

module.exports = resolvers;
