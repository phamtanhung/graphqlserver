const { gql } = require('apollo-server');

const typeDefs = gql`
  enum GenderType {
    female,
    male
  }
  type IdType {
    value: String
    name: String
  }
  type NameType {
    title: String
    first: String
    last: String
  }
  type StreetType {
    number: Int,
    name: String
  }
  type CoordinatesType {
    latitude: String,
    longitude: String
  }

  type TimezoneType {
    offset: String,
    description: String
  }
  
  type LocationType {
    street: StreetType
    city: String
    state: String
    country: String
    postcode: Int
    coordinates: CoordinatesType
    timezone: TimezoneType
  }

  type DobType {
    date: String,
    age: Int
  }

  type User {
    id: String
    username: String
    name: NameType
    email: String
    nat: String,
    location: LocationType,
    dob: DobType,
    gender: GenderType,
    createdAt: String
    updatedAt: String
  }

  input UserInput {
    id: String
    username: String
  }

  input NewUserInput {
    username: String!
    email: String!
    gender: GenderType
  }

  input UpdateUserInput {
    id: String
    username: String
    email: String
    gender: GenderType
  }

  input MostUserInput {
    limit: Int
  }
  
  type Query {
    users: [User]
    user(input: UserInput!): User
    getRandomUser: User
    getMostUsers(input: MostUserInput!): [User]
  }


  type Mutation {
    addUser(input: NewUserInput!): User
    updateUser(input: UpdateUserInput!): User
    removeUser(input: UserInput!): User
  }
`;

module.exports = typeDefs;