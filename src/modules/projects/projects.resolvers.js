
const resolvers = {
  Query: {
    projects: (parent, { input }, { models }) => {
      return models.Project.findAll();
    },
    project(parent, { input }, { models }) {
      return models.Project.findOne(input);
    }
  },
  Project: {
    members: (project, { input }, { models }) => {
      console.log('finding project members', project.members);
      if (!project.members) return [];
      return project.members.map(member => {
        console.log('finding user id', member);
        const user = models.User.findOne({
          id: member.id
        });
        return user;
      });
    }
  },
  Mutation: {
    addProject: (parent, { input }, { models }) => {
      console.log('parent', parent);
      console.log('input', input);
      const newProject = models.Project.create({ ...input })
      return newProject;
    },
    updateProject: (parent, { input }, { models }) => {
      console.log('parent', parent);
      console.log('input', input);
      const project = models.Project.update({ ...input })
      return project;
    },
    removeProject: (parent, { input }, { models }) => {
      const project = models.Project.remove({ ...input });
      return project;
    }
  }
}

module.exports = resolvers;
