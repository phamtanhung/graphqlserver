const projectTypeDefs = require('./projects.typedefs');
const projectResolvers = require('./projects.resolvers');

module.exports = {
  projectTypeDefs,
  projectResolvers
};