const { gql } = require('apollo-server');

const typeDefs = gql`
  
  type Project {
    id: ID!
    name: String,
    members: [User]
  }

  input ProjectInput {
    id: ID,
    name: String
  }

  input NewProjectInput {
    name: String
  }

  input UpdateProjectInput {
    id: String
    name: String,
    members: [UserInput]
  }

  extend type Query {
    projects: [Project]
    project(input: ProjectInput!): Project
  }

  extend type Mutation {
    addProject(input: NewProjectInput!): Project
    updateProject(input: UpdateProjectInput!): Project
    removeProject(input: ProjectInput!): Project
  }
`;

module.exports = typeDefs;