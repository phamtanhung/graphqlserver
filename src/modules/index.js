const { mergeModules } = require('apollo-modulizer');

const User = require('./users');

module.exports = { typeDefs, resolvers } = mergeModules([User]);