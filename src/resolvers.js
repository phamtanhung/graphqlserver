// Resolvers define the technique for fetching the types defined in the
const { userResolvers } = require('./modules/users');
const { projectResolvers } = require('./modules/projects');

module.exports = [userResolvers, projectResolvers];
