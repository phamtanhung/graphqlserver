const { RESTDataSource } = require('apollo-datasource-rest');
const nanoid = require('nanoid');
class UsersAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = 'https://randomuser.me/api';
  }

  async getRandomUser() {
    const data = await this.get('?results=1');
    if (!data || !data.results || data.results.length === 0) {
      return new Error('Can not found data');
    }
    const result = data.results[0];
    const id = nanoid()
    return { ...result, id };
  }

  async getMostUsers(limit = 10) {
    const data = await this.get(`?results=${limit}`);
    return data.results.map(result => {
      const id = nanoid()
      return {...result, id};
    });
  }
}

module.exports = UsersAPI;