const nanoid = require('nanoid');

const collectionName = 'projects';

const createProjectModel = db => {
  return {
    findOne(input) {
      return db.get(collectionName)
        .find({ ...input})
        .value()
    },

    findAll() {
      return db.get(collectionName)
        .take(50)
        .value()
    },

    create(project) {
      const newProject = {id: nanoid(), createdAt: Date.now(), ...project}
      db.get(collectionName)
        .push(newProject)
        .write()
  
      return newProject
    },
    update(input) {
      const { name, id, members } = input;
      
      const found = db.get(collectionName)
        .find({ id })
        .value();
      
      console.log('input', input);
      const payload = {
        ...found,
        members: [...found.members, ...members],
        name
      };

      return db.get(collectionName)
        .find({ id })
        .assign(payload)
        .write();
    },
  }
}

module.exports = createProjectModel
