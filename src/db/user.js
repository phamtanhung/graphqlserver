const nanoid = require('nanoid');

const createUserModel = db => {
  return {
    findOne(input) {
      const { username, id } = input;
      const user = {};
      if (username) user.username = username;
      if (id) user.id = id;
      return db.get('users')
        .find({...user})
        .value()
    },

    findAll(limit = 10) {
      return db.get('users')
        .take(limit)
        .value()
    },

    create(user) {
      const newUser = {
        ...user,
        id: {
          name: 'nanoid',
          value: nanoid()
        },
        createdAt: Date.now()
      };

      db.get('users')
        .push(newUser)
        .write()
  
      return newUser
    },
    update(input) {
      const { username, idValue, idName } = input;
      const user = {
        id: {

        }
      };

      if (username) user.username = username;
      if (idValue) user.id.value = idValue;
      if (idName) user.id.name = idName;
      
      const foundUser = db.get('users')
        .find({ ...user })
        .value();

      if (!idValue) user.id.value = foundUser.id.value;
      if (!idName && foundUser.id.name) user.id.name = foundUser.id.name;
      
      const updatedUser = {
          ...foundUser,
          ...input,
          ...user,
          updatedAt: Date.now()
        };

      const { idValue: value, idName: name, ...payload } = updatedUser;

        return db.get('users')
        .find({ ...foundUser })
        .assign(payload)
        .write();
    },
    remove(input) {
      const { username, idValue, idName } = input;
      const user = {};
      if (username) user.username = username;
      if (idValue || idName) user.id = {};
      if (idValue) user.id.value = idValue;
      if (idName) user.id.name = idName;
      
      const found = db.get('users')
        .find({ ...user }).value();
      
      if (!found) return new Error("Object not found!");

      db.get('users')
        .remove({ ...user }).value();

      return found;
    },
  }
}

module.exports = createUserModel
