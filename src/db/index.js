const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const adapter = new FileSync('src/db/db.json');

const db = low(adapter);

const createUserModel = require('./user');
const createProjectModel = require('./project');

module.exports = {
  models: {
    User: createUserModel(db),
    Project: createProjectModel(db),
  },
  db
};
